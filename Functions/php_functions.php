<?php

function edit_specific_record($filename, $oldString, $newString){
	
	$file = file($filename);
	
	for($i = 0 ; $i < count($file); $i++){
		$fileRow = $file[$i];
		if(strpos($fileRow, $oldString)){
			$file[$i] = str_replace($oldString,$newString,$fileRow);
		}
	}
	$string = implode($file);
	file_put_contents($filename,$string);
}

function add_new_repo($filename, $repoName, $contributors){
	
	$fp = fopen($filename, "a");
	
	$information = "\n"."repo ".$repoName."\n".'RW+'."\t\t= ".$contributors; 
	fputs($fp, $information);
	fclose($fp);
}

function remove_repo($filename, $repoName){
	
	$file = file($filename);
	
	for($i = 0 ; $i < count($file); $i++){
		$fileRow = $file[$i];
		$match1 = strpos($fileRow,$repoName);
		$match2 = strpos($fileRow,'repo');
		if($match1 !== false and $match2 !== false){
			$file[$i] = "";
			$q = $i;
			while(strpos($file[$q], "repo") === false 
					and $q != count($file)
// 					or $q+1 == count($file)
// 					or $file[$q+1] = ""
					){
				$file[$q] = "";
				$q++;
			}
		}
	}
	$string = implode($file);
	file_put_contents($filename,$string);
}

function add_group($filename, $groupName, $contributors){
	
	$fp = fopen($filename, "a");
	
	$information = "\n"."@".$groupName."\t\t= ".$contributors;
	fputs($fp, $information);
	fclose($fp);
}

function edit_group($filename, $contributor, $oldGroup, $newGroup){
	
	$file = file($filename);
	
	for($i = 0 ; $i < count($file); $i++){
		$fileRow = $file[$i];
		if(strpos($fileRow, "@".$oldGroup) !== false){
			$file[$i] = str_replace(" ".$contributor,"",$fileRow);
		}
	}
	for($q = 0; $q <count($file); $q++){
		$fileRow = $file[$q];
		if(strpos($fileRow, "@".$newGroup) !== false){
			$file[$q] = str_replace("= ","= ".$contributor." ",$fileRow);
		}
	}
	
	$string = implode($file);
	file_put_contents($filename,$string);
}

function delete_gruup($filename, $groupName){
	
	$file = file($filename);
	
	for($i = 0 ; $i < count($file); $i++){
		$fileRow = $file[$i];
		if(strpos($fileRow,"@".$groupName) !== false){
			$file[$i] = "";
		}
	}
	$string = implode($file);
	file_put_contents($filename,$string);
}

?>