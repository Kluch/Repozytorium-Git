<?php
	$host = "localhost";
	$db_user = "root";
	$db_password = "";
	$db_name = "git_management";
	
class Table {
	const Students = 'students';
    const Group = 'students_groups';
    const Group_Members = 'group_members';
    const Labs = 'labs';
    const Lab_Members = 'lab_members';
    const Subject = 'subject';
    const Permissions = 'permissions';
    const Users = 'users';
}
?>