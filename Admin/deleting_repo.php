<?php 
	session_start();		
	if(!isset($_SESSION['isOnline']) || $_SESSION['isAdmin'] != 1){
		header('Location:../index.php');
		exit();
	}

	if(!isset($_POST['radiobutton'][0])){
		header('Location: delete_repository.php');
		$_SESSION['userOper'] = '<span style="color:red">Nie zaznaczono przedmiotu do usuniecia</span>';
		exit();
	}
	
require_once '../Database/connect.php';

$connect = @new mysqli($host,$db_user,$db_password,$db_name);
if($connect->connect_errno != 0){
	echo "Error: ".$connect->connect_errno."Opis".$connect->connect_error;
}else{
	$name = $_POST['radiobutton'][0];
	
	$table = Table::Subject;
	$query = "DELETE FROM $db_name.$table WHERE name='$name'";

	$command = mysql_query($query) or die(mysql_error());
	if($command){
		$_SESSION['userOper'] = '<span style="color:green">Usunięto przedmiot</span>';
	}else{
		$_SESSION['userOper'] = '<span style="color:red">Nie usunieto przedmiotu</span>';
	}
	header('Location: delete_repository.php');
	
	$connect->close();
	}

?>