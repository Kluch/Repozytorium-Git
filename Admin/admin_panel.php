<?php 
	session_start();		
	if(!isset($_SESSION['isOnline']) || $_SESSION['isAdmin'] != 1){
		header('Location:../index.php');
		exit();
	}
?>

<!DOCTYPE HTML>
<html lang="PL">
	<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title></title>
	<meta name="description" content=""/>
	<meta name="keywords" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	</head>
	<body>

	<?php 
		echo "Witaj ".$_SESSION['isAdmin']."!<br><br>";
 	?>
 	
 	<?php 
	if(isset($_SESSION['userOper'])){
		echo $_SESSION['userOper']."<br><br>";
		unset($_SESSION['userOper']);
	}
	?>
 	
 	
 		<form name="Add_New_Repo" action="add_new_repository.php" method="post">
			<input type="submit" value="Testuj plik">
		</form>
		 <form name="Delete_Repo" action="delete_repository.php" method="post">
			<input type="submit" value="Usuń repozytorium">
		</form>
		<form name="Add_subject" action="add_subject.php" method="post">
			<input type="submit" value="Dodaj nowy przedmiot">
		</form>
		<form name="Add_User" action="add_new_user.php" method="post">
			<input type="submit" value="Dodaj uzytkownika">
		</form>
		<form name="Del_User" action="del_user.php" method="post">
			<input type="submit" value="Usuń uzytkownika">
		</form>
		<form name="Permissions" action="permissions.php" method="post">
			<input type="submit" value="Nadaj uprawnienia">
		</form>
 	
 	<?php 
 		echo "<br><br><a href='../logout.php'>Wyloguj</a>";
 	?>

	</body>
</html>