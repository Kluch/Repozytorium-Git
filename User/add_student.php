<?php 
	session_start();		
	if(!isset($_SESSION['isOnline']) || $_SESSION['isAdmin'] != 0){
		header('Location:../index.php');
		exit();
	}
?>

<!DOCTYPE HTML>
<html lang="PL">
	<head>
	<meta charset="UTF-8">
	<title></title>
	<meta name="description" content=""/>
	<meta name="keywords" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<script type="text/javascript">
	function ValidateEmail(address){
		var reg = /^([0-9]{6})+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		if(reg.test(address) == true) {
		return true;
		}else{	
			return false;
		}
	}

	function ValidateIndex(index){
		var reg = /^[0-9]{6}$/;
		if(reg.test(index) == true){
			return true;
		}else{
			return false;
		}
	}
	
	function validateForm(AForm){
		var tekst='';
		if (AForm.name.value==""){
			tekst=tekst+"Wpisz swoje imię\n";
		}
		if (AForm.surname.value==""){
			tekst=tekst+"Wpisz swoje nazwisko\n";
		}
		if (AForm.login.value==""){
			tekst=tekst+"Wpisz swój login\n";
		}
		if (AForm.password.value==""){
			tekst=tekst+"Wpisz swoje hasło\n";
		}
		if (!ValidateEmail(AForm.email.value)){
			tekst=tekst+"Nieprawidłowy format adresu e-mail\n";
		}
		if (!ValidateIndex(AForm.index.value)){
			tekst=tekst+"Nieprawidłowy format indeksu\n";
		}
		if (tekst!="") {
			alert ("WYSTAPIŁY NASTĘPUJĄCE BŁĘDY\n"+tekst);
			return false;
		}else{
			return true;
		}
	}
	
	</script>
	</head>
	<body>

	Dodawanie nowego studenta<br><br>
	<form name="student_form" action="adding_student.php" method="post" onsubmit="return validateForm(this);">
		Login:<br>
		<input type="text" name="login"><br>
		Hasło:<br>
		<input type="password" name="password"><br>
		Imię:<br>
		<input type="text" name="name"><br>
		Nazwisko:<br>
		<input type="text" name="surname"><br>
		Email:<br>
		<input type="text" name="email"><br>
		Nr. indeksu:<br>
		<input type="text" name="index"><br>
		
		<br><input type="submit" value="Dodaj">
	</form>
	
	
<!-- 	<form name="Add_New_Repo" action="add_new_repository.php" method="post"> -->
<!-- 		<input type="submit" value="Dodaj studenta"> -->
<!-- 	</form> -->
	

	<br><a href="javascript: history.back()">
		<input type="submit" value="Powrót">
	</a>
	<br><br><a href='../logout.php'>Wyloguj</a>
	</body>
</html>