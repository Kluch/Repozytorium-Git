<?php 
	session_start();		
	if(!isset($_SESSION['isOnline']) || $_SESSION['isAdmin'] != 0){
		header('Location:../index.php');
		exit();
	}
?>

<!DOCTYPE HTML>
<html lang="PL">
	<head>
	<meta charset="UTF-8">
	<title></title>
	<meta name="description" content=""/>
	<meta name="keywords" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<script type="text/javascript">
	</script>
	</head>
	<body>

	Strona laboratorium <?php echo $_SESSION['subject_name']?><br><br>
	Ścieżka: <?php echo $_SESSION['subject_name']."/"?>
	
	<br><br>
	 	<?php 
	if(isset($_SESSION['userOper'])){
		echo $_SESSION['userOper']."<br><br>";
		unset($_SESSION['userOper']);
	}
	?>

		<form name="New_lab" action="add_lab.php" method="post">
			<input type="submit" value="Stwórz laboratorium">
		</form>
		<form name="Delete_lab" action="remove_lab.php" method="post">
			<input type="submit" value="Usuń laboratorium">
		</form>

		<input type="submit" value="Powrót" onclick="window.location.href='user_panel.php' ">

	<br><br><a href='../logout.php'>Wyloguj</a>

	</body>
</html>