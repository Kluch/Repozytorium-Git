<?php 
	session_start();		
	if(!isset($_SESSION['isOnline']) || $_SESSION['isAdmin'] != 0){
		header('Location:../index.php');
		exit();
	}
?>
	
<?php 
	require_once '../Database/connect.php';

	$connect = @new mysqli($host,$db_user,$db_password,$db_name);
	if($connect->connect_errno != 0){
		echo "Error: ".$connect->connect_errno."Opis".$connect->connect_error;
	}else{
		$type = substr($_POST['radio'][0],0,1);
		$id = substr($_POST['radio'][0], 1);
		$labs = Table::Labs;
		$subject = Table::Subject;
		$_SESSION['id'] = $id;
		
		if($type == 's'){
			$query = "SELECT name FROM $db_name.$subject WHERE ID = $id";
			$command = mysql_query($query) or die(mysql_error());
		
			while($rek = mysql_fetch_array($command)) {
				$_SESSION['subject_name'] = $rek['name'];
			}
			header('Location: subject_page.php');
			
		}elseif($type == 'l'){
			$query = "SELECT s.name as subject, l.name as lab FROM $db_name.$labs as l, $db_name.$subject as s 
					  WHERE l.ID = $id AND s.ID = l.ID_subject";
			$command = mysql_query($query) or die(mysql_error());
			
			while($rek = mysql_fetch_array($command)) {
				$_SESSION['subject_name'] = $rek['subject'];
				$_SESSION['lab_name'] = $rek['lab'];
			}
			header('Location: lab_page.php');
		}
		
		$connect->close();
	}

?>