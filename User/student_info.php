<?php 
	session_start();		
	if(!isset($_SESSION['isOnline']) || $_SESSION['isAdmin'] != 0){
		header('Location:../index.php');
		exit();
	}
?>

<?php 
	require_once '../Database/connect.php';
	$connect = @new mysqli($host,$db_user,$db_password,$db_name);
	$students = Table::Students;
	$students_lab = Table::Lab_Members;
	$id = $_POST['radio'][0];
	
	$query = "SELECT name, surname, login, indeks, email FROM $db_name.$students WHERE ID = $id;";
	$result = mysql_query($query) or die("Something is not right");
	
	mysql_close();	
	while($rek1 = mysql_fetch_array($result)) {
		$login		= $rek1['login'];
		$surname 	= $rek1['surname'];
		$name		= $rek1['name'];
		$index		= $rek1['indeks'];
		$email		= $rek1['email'];
	}

?>

<!DOCTYPE HTML>
<html lang="PL">
	<head>
	<meta charset="UTF-8">
	<title></title>
	<meta name="description" content=""/>
	<meta name="keywords" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<script type="text/javascript">
	function ValidateEmail(address){
		var reg = /^([0-9]{6})+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		if(reg.test(address) == true) {
		return true;
		}else{	
			return false;
		}
	}

	function ValidateIndex(index){
		var reg = /^[0-9]{6}$/;
		if(reg.test(index) == true){
			return true;
		}else{
			return false;
		}
	}
	
	function validateForm(AForm){
		var tekst='';
		if (AForm.name.value==""){
			tekst=tekst+"Wpisz swoje imię\n";
		}
		if (AForm.surname.value==""){
			tekst=tekst+"Wpisz swoje nazwisko\n";
		}
		if (AForm.login.value==""){
			tekst=tekst+"Wpisz swój login\n";
		}
		if (AForm.password.value==""){
			tekst=tekst+"Wpisz swoje hasło\n";
		}
		if (!ValidateEmail(AForm.email.value)){
			tekst=tekst+"Nieprawidłowy format adresu e-mail\n";
		}
		if (!ValidateIndex(AForm.index.value)){
			tekst=tekst+"Nieprawidłowy format indeksu\n";
		}
		if (tekst!="") {
			alert ("WYSTAPIŁY NASTĘPUJĄCE BŁĘDY\n"+tekst);
			return false;
		}else{
			return true;
		}
	}
	
	</script>
	</head>
	<body>

	Informacje<br><br>
	<form name="student_form" action="edit_student.php" method="post" onsubmit="return validateForm(this);">
		
		<p>Login <?php echo $login?></p>
		<p>Imię <input type="text" name="name" value="<?php echo $name ?>"></p>
		<p>Nazwisko <input type="text" name="surname" value="<?php echo $surname ?>"></p>
		<p>Indeks <input type="text" name="index" value="<?php echo $index ?>"></p>
		<p>Email <input type="text" name="email" value="<?php echo $email ?>"></p>
		<!-- Uzupełnić -->
		<p>Grupy <?php echo "UZUPEŁNIC!!!"?> </p>
		<!-- /Uzupełnić -->
		<br>
		<p>Ewentualne nowe hasło <input type="text" name="pass1"></p>
		<p>Nazwisko <input type="text" name="pass2"></p>
	
		<input type="hidden" name="id" value="<?php echo $id ?>">
		<br><input type="submit" disabled="disabled" value="Edytuj">
	</form>
	

	<br><a href="javascript: history.back()">
		<input type="submit" value="Powrót">
	</a>
	<br><br><a href='../logout.php'>Wyloguj</a>
	</body>
</html>