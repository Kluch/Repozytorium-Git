<?php 
	session_start();		
	if(!isset($_SESSION['isOnline']) || $_SESSION['isAdmin'] != 0){
		header('Location:../index.php');
		exit();
	}

	if(!isset($_POST['check'][0])){
		header('Location: remove_lab.php');
		$_SESSION['userOper'] = '<span style="color:red">Nie zaznaczono laboratorium do usuniecia</span>';
		exit();
	}
	
require_once '../Database/connect.php';
$permission = Table::Permissions;
$lab = Table::Labs;

$connect = @new mysqli($host,$db_user,$db_password,$db_name);
if($connect->connect_errno != 0){
	echo "Error: ".$connect->connect_errno."Opis".$connect->connect_error;
}else{
	for($i = 0 ; $i < count($_POST['check']); $i++){
		
		$id = $_POST['check'][$i];
		$query1 = "DELETE FROM $db_name.$permission WHERE ID_lab = $id";
		$query2 = "DELETE FROM $db_name.$lab WHERE ID = $id";
		
		$command1 = mysql_query($query1) or die(mysql_error());
		$command2 = mysql_query($query2) or die(mysql_error());
		
		if(!$command1 or !$command2){
			$_SESSION['userOper'] = '<span style="color:red">Wystąpił błąd, nie usunięto laboratorium</span>';
			header('Location: remove_lab.php');
			exit();
		}
	}

	header('Location: subject_page.php');
	$_SESSION['userOper'] = '<span style="color:green">Pomyślnie usunięto przedmioty</span>';
	$connect->close();
	}

?>
