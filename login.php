<?php 
	session_start();
	
	if(!isset($_POST['login']) || !isset($_POST['password'])){
		header('Location:index.php');
		exit();
	}
	
	
	require_once 'Database/connect.php';

	$connect = @new mysqli($host,$db_user,$db_password,$db_name);
	if($connect->connect_errno != 0){
		echo "Error: ".$connect->connect_errno."Opis".$connect->connect_error;
	}else{
		$login = $_POST['login'];
		$password = ($_POST['password']);
		
		$login = htmlentities($login,ENT_QUOTES, "UTF-8");//do kodowania 
		$password = htmlentities($password,ENT_QUOTES, "UTF-8");

		if($user_result = @$connect->query(sprintf("SELECT * FROM users where login ='%s' AND password = '%s'",
				mysqli_real_escape_string($connect, $login),
				mysqli_real_escape_string($connect, $password)))){//do kodowania
			$user_count = $user_result->num_rows;
			if($user_count>0){
				$wiersz = $user_result->fetch_assoc();
				$_SESSION['isOnline'] = true;
				$_SESSION['isAdmin'] = $wiersz['admin'];
				
				$_SESSION['user_id'] = $wiersz['id'];	
				$_SESSION['user_name'] = $wiersz['name'];
				
				if($wiersz['admin'] == 1){
					header('Location:Admin/admin_panel.php');
				}elseif($wiersz['admin'] == 0){
					header('Location:User/user_panel.php');
				}

				unset($_SESSION['blad']); //usu� z sesji
				$user_result->close();
			}else{
				$_SESSION['blad'] = '<span style="color:red">Nieprawidłowy login lub hasło</span>';
				header('Location: index.php');
			}
		}
		
		$connect->close();
	}

?>